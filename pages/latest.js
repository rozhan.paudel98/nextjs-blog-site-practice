import React from "react";
import Link from "next/link";
export default function Latest() {
  return (
    <div>
      <h4>All the Latest Posts</h4>
      <Link href="/">Go to Home</Link>
    </div>
  );
}
