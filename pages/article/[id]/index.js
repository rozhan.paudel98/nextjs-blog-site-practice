import { useRouter } from "next/router";
import Head from "next/head";
export default function SingleArticle({ post }) {
  const router = useRouter();
  const { id } = router.query;

  return (
    <div>
      <Head>
        <title>Article No {id}</title>
      </Head>
      <h2>this is an article {id}</h2>
      <p>{post.title}</p>
    </div>
  );
}

export const getServerSideProps = async (context) => {
  const post = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${context.params.id}`
  );
  const newPost = await post.json();

  return {
    props: {
      post: newPost,
    },
  };
};
