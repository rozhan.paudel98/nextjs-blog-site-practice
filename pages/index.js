import axios from "axios";
import Head from "next/head";
import Image from "next/image";
import Header from "../components/Header";
import Card from "../components/Card";
import styles from "../styles/Home.module.css";

export default function Home({ result }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Real information</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <h1>First blogging platform from nepal</h1>
      {result.map((item, i) => {
        return <Card data={item} />;
      })}
    </div>
  );
}

export const getStaticProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts");
  const result = await res.json();
  return {
    props: { result: result ? result : [] },
  };
};
