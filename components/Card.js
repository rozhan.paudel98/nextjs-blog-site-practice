import Link from "next/link";
export default function Card({ data }) {
  return (
    <div>
      <h2>{data.title}</h2>
      <p>{data.body}</p>
      <Link href="/article/[id]" as={`article/${data.id}`}>
        See this one
      </Link>
    </div>
  );
}
